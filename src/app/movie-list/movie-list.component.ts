import { Component, OnInit } from '@angular/core';
import { Movie } from '../movie';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {

  movies: Movie[] = [
    new Movie("Parasite", "2020", "Drama, Mystery", "assets/images/m1.jpg", 7.8),
    new Movie("Aliens", "1986", "Horror, SciFi", "assets/images/m8.jpg", 8.8),
    new Movie("Shawshank Redemption", "1994", "Drama", "assets/images/m5.jpg", 6.6),
    new Movie("The Joker", "2019", "Drama, Horror", "assets/images/m4.jpg", 5.6)
  ]

  constructor() { }

  ngOnInit(): void {
  }

  deleteMovieFromArray(movie: Movie){
    console.log("the movie to delete is " + movie.title);
      let index = this.movies.indexOf(movie);
      this.movies.splice(index, 1);
  }

}
