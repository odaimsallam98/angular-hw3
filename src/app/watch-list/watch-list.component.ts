import { Component, OnInit } from '@angular/core';
import { Movie } from '../movie';

@Component({
  selector: 'app-watch-list',
  templateUrl: './watch-list.component.html',
  styleUrls: ['./watch-list.component.css']
})
export class WatchListComponent implements OnInit {

  watch_list: Movie[] = [];

  constructor() { }

  ngOnInit(): void {
  }
  addToList(fmovie: Movie){
    console.log("the movie to be added " + fmovie.title);
      this.watch_list.push(fmovie);
  }


}
