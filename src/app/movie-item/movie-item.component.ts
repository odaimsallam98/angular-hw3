import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Movie } from '../movie';

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.css']
})
export class MovieItemComponent implements OnInit {

  _movie: Movie;

  constructor() { }

  ngOnInit(): void {
  }

  @Input()
  set movie(movie: Movie){
    this._movie = movie;
  }

  @Output() deleteMovie = new EventEmitter<Movie>();

  delete(){
    this.deleteMovie.emit(this._movie);
  }

  @Output() Add_To_Watch_List = new EventEmitter<Movie>();

  addToWatchList(){
    this.Add_To_Watch_List.emit(this._movie);
  }


}
